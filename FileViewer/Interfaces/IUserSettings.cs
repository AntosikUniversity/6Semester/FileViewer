﻿namespace FileViewer.Interfaces
{
    public interface IUserSettings
    {
        string EncodingKey { get; set; }
        bool RequestKeyEachTime { get; set; }
        int LastCommandsCount { get; set; }
    }
}