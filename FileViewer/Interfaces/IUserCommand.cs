﻿using FileViewer.Helpers;

namespace FileViewer.Interfaces
{
    public interface IUserCommand
    {
        ModalTypesEnum CommandName { get; set; }
        string TargetName { get; set; }
    }
}