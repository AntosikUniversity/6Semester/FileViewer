﻿using System.Collections.Generic;

namespace FileViewer.Interfaces
{
    public interface IUser
    {
        string Login { get; set; }
        string Password { get; set; }
        IUserSettings Settings { get; set; }
        IEnumerable<IUserCommand> LastCommands { get; set; }
    }
}