﻿namespace FileViewer.Helpers
{
    public enum ModalTypesEnum
    {
        CreateFile,
        CreateDirectory,
        Copy,
        Move,
        Delete,
        Rename,
        ShowAttributes,
        Observe,
        TripleDES,
        Rijndael,
        RC2,
        RSA,
        Sign
    }
}