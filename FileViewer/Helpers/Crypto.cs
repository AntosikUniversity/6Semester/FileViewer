﻿using System;
using System.IO;
using System.Security.Cryptography;

namespace FileViewer.Helpers
{
    public static class Crypto
    {
        public static void EncryptFileTripleDES(string inputPath, string outputPath)
        {
            // Create the file streams to handle the input and output files.
            FileStream fin = new FileStream(inputPath, FileMode.Open, FileAccess.Read);
            FileStream fout = new FileStream(outputPath, FileMode.OpenOrCreate, FileAccess.Write);
            fout.SetLength(0);

            // Create variables to help with read and write.
            byte[] bin = new byte[100];     // This is intermediate storage for the encryption.
            long rdlen = 0;                 // This is the total number of bytes written.
            long totlen = fin.Length;       // This is the total length of the input file.
            int len;                        // This is the number of bytes to be written at a time.

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.GenerateKey();
            tdes.GenerateIV();

            CryptoStream encStream = new CryptoStream(fout, tdes.CreateEncryptor(tdes.Key, tdes.IV), CryptoStreamMode.Write);

            Console.WriteLine("Encrypting...");

            // Read from the input file, then encrypt and write to the output file.
            while (rdlen < totlen)
            {
                len = fin.Read(bin, 0, 100);
                encStream.Write(bin, 0, len);
                rdlen = rdlen + len;
            }

            Console.WriteLine("Encrypted!");
            encStream.Close();
        }

        public static void EncryptFileRijndael(string inputPath, string outputPath)
        {
            // Create the file streams to handle the input and output files.
            FileStream fin = new FileStream(inputPath, FileMode.Open, FileAccess.Read);
            FileStream fout = new FileStream(outputPath, FileMode.OpenOrCreate, FileAccess.Write);
            fout.SetLength(0);

            // Create variables to help with read and write.
            byte[] bin = new byte[100];     // This is intermediate storage for the encryption.
            long rdlen = 0;                 // This is the total number of bytes written.
            long totlen = fin.Length;       // This is the total length of the input file.
            int len;                        // This is the number of bytes to be written at a time.

            Rijndael rij = Rijndael.Create();
            rij.GenerateKey();
            rij.GenerateIV();

            CryptoStream encStream = new CryptoStream(fout, rij.CreateEncryptor(rij.Key, rij.IV), CryptoStreamMode.Write);

            Console.WriteLine("Encrypting...");

            // Read from the input file, then encrypt and write to the output file.
            while (rdlen < totlen)
            {
                len = fin.Read(bin, 0, 100);
                encStream.Write(bin, 0, len);
                rdlen = rdlen + len;
            }

            Console.WriteLine("Encrypted!");
            encStream.Close();
        }

        public static void EncryptFileRC2(string inputPath, string outputPath)
        {
            // Create the file streams to handle the input and output files.
            FileStream fin = new FileStream(inputPath, FileMode.Open, FileAccess.Read);
            FileStream fout = new FileStream(outputPath, FileMode.OpenOrCreate, FileAccess.Write);
            fout.SetLength(0);

            // Create variables to help with read and write.
            byte[] bin = new byte[100];     // This is intermediate storage for the encryption.
            long rdlen = 0;                 // This is the total number of bytes written.
            long totlen = fin.Length;       // This is the total length of the input file.
            int len;                        // This is the number of bytes to be written at a time.

            RC2 rc2 = RC2.Create();
            rc2.GenerateKey();
            rc2.GenerateIV();

            CryptoStream encStream = new CryptoStream(fout, rc2.CreateEncryptor(rc2.Key, rc2.IV), CryptoStreamMode.Write);

            Console.WriteLine("Encrypting...");

            // Read from the input file, then encrypt and write to the output file.
            while (rdlen < totlen)
            {
                len = fin.Read(bin, 0, 100);
                encStream.Write(bin, 0, len);
                rdlen = rdlen + len;
            }

            Console.WriteLine("Encrypted!");
            encStream.Close();
        }

        public static void EncryptFileRSA(string inputPath, string outputPath, RSAParameters RSAKey, bool DoOAEPPadding = false)
        {
            // Create the file streams to handle the input and output files.
            FileStream fin = new FileStream(inputPath, FileMode.Open, FileAccess.Read);
            FileStream fout = new FileStream(outputPath, FileMode.OpenOrCreate, FileAccess.Write);
            fout.SetLength(0);

            // Create variables to help with read and write.
            byte[] bin = new byte[100];     // This is intermediate storage for the encryption.
            long rdlen = 0;                 // This is the total number of bytes written.
            long totlen = fin.Length;       // This is the total length of the input file.
            int len;                        // This is the number of bytes to be written at a time.

            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
            rsa.ImportParameters(RSAKey);

            BinaryWriter encStream = new BinaryWriter(fout);

            Console.WriteLine("Encrypting...");

            // Read from the input file, then encrypt and write to the output file.
            while (rdlen < totlen)
            {
                len = fin.Read(bin, 0, 100);
                var encryptedBin = rsa.Encrypt(bin, DoOAEPPadding);
                encStream.Write(encryptedBin, 0, len);
                rdlen = rdlen + len;
            }

            Console.WriteLine("Encrypted!");
            encStream.Close();
        }
    }
}