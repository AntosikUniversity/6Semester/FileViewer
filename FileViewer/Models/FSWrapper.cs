﻿using System.Collections.Generic;
using FileViewer.Interfaces;
using FileViewer.ViewModels;

namespace FileViewer.Models
{
    public class FsWrapper : ViewModelBase, INodeWrapper
    {
        private List<INodeWrapper> _children;
        private INode _item;

        public FsWrapper(INode item)
        {
            Item = item;
            Children = GenerateChildren();
            if (Item is IDirectory directory)
            {
                directory.Directories.CollectionChanged += (sender, args) => Children = GenerateChildren();
                directory.Files.CollectionChanged += (sender, args) => Children = GenerateChildren();
            }
        }

        public INode Item
        {
            get => _item;
            set => Change(ref _item, value);
        }

        public string Name
        {
            get
            {
                var type = Item.GetType();
                var propertyInfo = type.GetProperty("Name");
                return propertyInfo?.GetValue(Item, null).ToString();
            }
        }

        public string Path
        {
            get
            {
                var type = Item.GetType();
                var propertyInfo = type.GetProperty("Path");
                return propertyInfo?.GetValue(Item, null).ToString();
            }
        }

        public List<INodeWrapper> Children
        {
            get => _children;
            set => Change(ref _children, value);
        }

        public ref INode GetNode()
        {
            return ref _item;
        }

        private List<INodeWrapper> GenerateChildren()
        {
            var list = new List<INodeWrapper>();

            if (!(Item is IDirectory)) return list;

            var dir = (IDirectory) Item;

            if (dir.Directories != null)
                foreach (var directory in dir.Directories)
                    list.Add(new FsWrapper(directory));
            if (dir.Files != null)
                foreach (var file in dir.Files)
                    list.Add(new FsWrapper(file));

            return list;
        }
    }
}