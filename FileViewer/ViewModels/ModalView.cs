﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Windows.Forms;
using System.Windows.Input;
using FileViewer.Helpers;
using FileViewer.Interfaces;
using FileViewer.Models;
using Directory = System.IO.Directory;
using File = System.IO.File;

namespace FileViewer.ViewModels
{
    public class ModalView : ViewModelBase, IModal
    {
        private string _description;
        private string _name;
        private ICommand _onOk;
        private string _value;

        public ModalView(ModalTypesEnum type, object target)
        {
            ; // false = file, true = dir;
            if (!(target is FsWrapper)) return;

            var targetWrapper = (FsWrapper) target;
            var path = targetWrapper.Path;
            var targetType = (targetWrapper.Item is IDirectory);

            switch (type)
            {
                case ModalTypesEnum.CreateFile:
                {
                    Name = "Create new file...";
                    Value = "New File";
                    Description = "Enter file name:";
                    OnOk = new Command(filename =>
                    {
                        var filenameStr = filename as string;
                        if (filenameStr == null) return;

                        if (filenameStr == "")
                        {
                            MessageBox.Show(@"File name can't be empty!");
                            return;
                        }
                        var newPath = targetType ? $"{path}\\{filenameStr}" : $"{Path.GetDirectoryName(path)}\\{filenameStr}";
                        if (File.Exists(newPath))
                            ConfirmModal("New file", "Replace existing file?",
                                () => File.Create(newPath));
                        else
                            File.Create(newPath);
                    });
                    break;
                }
                case ModalTypesEnum.CreateDirectory:
                {
                    Name = "Create new directory...";
                    Value = "New Folder";
                    Description = "Enter directory name:";
                    OnOk = new Command(dirname =>
                    {
                        var dirnameStr = dirname as string;
                        if (dirnameStr == null) return;

                        if (dirnameStr == "")
                        {
                            MessageBox.Show(@"Folder name can't be empty!");
                            return;
                        }
                        var newPath = targetType ? $"{path}\\{dirnameStr}" : $"{Path.GetDirectoryName(path)}\\{dirnameStr}";
                        if (Directory.Exists(newPath))
                            MessageBox.Show(@"Directory already exists!");
                        else
                            Directory.CreateDirectory(newPath);
                    });
                    break;
                }
                case ModalTypesEnum.Copy:
                {
                    Name = "Copy to...";
                    Value = path;
                    Description = "Enter path to folder:";
                    OnOk = new Command(toPath =>
                    {
                        var toPathStr = toPath as string;
                        if (toPathStr == null) return;
                        if (targetType)
                        {
                            if (Directory.Exists(toPathStr))
                                ConfirmModal("Copy folder", "Replace existing files?",
                                    () => CopyDir(path, toPathStr)
                                );
                            else
                                CopyDir(path, toPathStr);
                        }
                        else
                        {
                            if (Directory.Exists(toPathStr))
                                ConfirmModal("Copy file", "Replace existing file?",
                                    () => File.Copy(path, toPathStr, true)
                                );
                            else
                                File.Copy(path, toPathStr, false);
                        }
                    });
                    break;
                }
                case ModalTypesEnum.Move:
                {
                    var pathArr = path.Split('\\');
                    var parentPath = string.Join("\\", pathArr.Take(pathArr.Length - 1));
                    var name = pathArr.Last();

                    Name = "Move to...";
                    Value = parentPath;
                    Description = "Enter path to folder:";
                    OnOk = new Command(toPath =>
                    {
                        var toPathStr = toPath as string;
                        if (toPathStr == null) return;

                        var newPath = $"{toPathStr}\\{name}";
                        MoveFile(targetType, path, newPath);
                    });
                    break;
                }
                case ModalTypesEnum.Delete:
                {
                    Name = "Delete...";
                    Value = "";
                    Description = "Click button, if you want delete this.";
                    OnOk = new Command(toPath =>
                    {
                        if (targetType) Directory.Delete(path, true);
                        else File.Delete(path);
                    });
                    break;
                }
                case ModalTypesEnum.Rename:
                {
                    var pathArr = path.Split('\\');
                    var parentPath = string.Join("\\", pathArr.Take(pathArr.Length - 1));
                    Name = "Rename...";
                    Value = "";
                    Description = "Click button, if you want rename this.";
                    OnOk = new Command(newName =>
                    {
                        var newNameStr = newName as string;
                        if (newNameStr == null) return;

                        if (newNameStr == "")
                        {
                            MessageBox.Show(@"Name can't be empty!");
                            return;
                        }
                        var newPath = $"{parentPath}\\{newNameStr}";
                        MoveFile(targetType, path, newPath);
                    });
                    break;
                }
                case ModalTypesEnum.ShowAttributes:
                {
                    if (targetType)
                    {
                        var dirInfo = new DirectoryInfo(path);
                        Value =
                            $"Name: {dirInfo.Name} \nPath: {dirInfo.FullName} \nCreated at:{dirInfo.CreationTime} \nLast access:{dirInfo.LastAccessTime}";
                    }
                    else
                    {
                        var fileInfo = new FileInfo(path);
                        Value =
                            $"Name: {fileInfo.Name} \nLength: {fileInfo.FullName} \nCreated at:{fileInfo.CreationTime} \nLast access:{fileInfo.LastAccessTime}";
                    }
                    Name = "Attributes...";
                    Description = "Attributes:";
                    break;
                }
                case ModalTypesEnum.Observe:
                {
                    var fw = new FileSystemWatcher
                    {
                        Path = path,
                        IncludeSubdirectories = true
                    };
                    Name = "Observing...";
                    Value = "";
                    Description = "Log:";
                    OnOk = new Command(newName => { fw.Dispose(); });
                    fw.Changed += Logging;
                    fw.Created += Logging;
                    fw.Deleted += Logging;
                    fw.Renamed += Logging;
                    fw.EnableRaisingEvents = true;

                    void Logging(object sender, FileSystemEventArgs e)
                    {
                        Value += $"{e.Name} - {e.ChangeType} \n";
                    }

                    break;
                }
                case ModalTypesEnum.TripleDES:
                {
                    Name = "Encrypt file using TripleDES...";
                    Value = "";
                    Description = "Enter encrypted file name:";
                    OnOk = new Command(filename =>
                    {
                        var filenameStr = filename as string;
                        if (filenameStr == null) return;

                        if (filenameStr == "")
                        {
                            MessageBox.Show(@"File name can't be empty!");
                            return;
                        }
                        var newPath = targetType ? $"{path}\\{filenameStr}" : $"{Path.GetDirectoryName(path)}\\{filenameStr}";
                        if (File.Exists(newPath))
                            ConfirmModal("File exists", "Override existing file?",
                                () => Crypto.EncryptFileTripleDES(path, newPath));
                        else
                            Crypto.EncryptFileTripleDES(path, newPath);
                    });
                    break;
                }
                case ModalTypesEnum.Rijndael:
                {
                    Name = "Encrypt file using Rijndael...";
                    Value = "";
                    Description = "Enter encrypted file name:";
                    OnOk = new Command(filename =>
                    {
                        var filenameStr = filename as string;
                        if (filenameStr == null) return;

                        if (filenameStr == "")
                        {
                            MessageBox.Show(@"File name can't be empty!");
                            return;
                        }
                        var newPath = targetType ? $"{path}\\{filenameStr}" : $"{Path.GetDirectoryName(path)}\\{filenameStr}";
                        if (File.Exists(newPath))
                            ConfirmModal("File exists", "Override existing file?",
                                () => Crypto.EncryptFileRijndael(path, newPath));
                        else
                            Crypto.EncryptFileRijndael(path, newPath);
                    });
                    break;
                }
                case ModalTypesEnum.RC2:
                {
                    Name = "Encrypt file using RC2...";
                    Value = "";
                    Description = "Enter encrypted file name:";
                    OnOk = new Command(filename =>
                    {
                        var filenameStr = filename as string;
                        if (filenameStr == null) return;

                        if (filenameStr == "")
                        {
                            MessageBox.Show(@"File name can't be empty!");
                            return;
                        }
                        var newPath = targetType ? $"{path}\\{filenameStr}" : $"{Path.GetDirectoryName(path)}\\{filenameStr}";
                        if (File.Exists(newPath))
                            ConfirmModal("File exists", "Override existing file?",
                                () => Crypto.EncryptFileRC2(path, newPath));
                        else
                            Crypto.EncryptFileRC2(path, newPath);
                    });
                    break;
                }
                case ModalTypesEnum.RSA:
                {
                    Name = "Encrypt file using RSA...";
                    Value = "";
                    Description = "Enter encrypted file name:";
                    OnOk = new Command(filename =>
                    {
                        var filenameStr = filename as string;
                        if (filenameStr == null) return;

                        if (filenameStr == "")
                        {
                            MessageBox.Show(@"File name can't be empty!");
                            return;
                        }
                        var newPath = targetType ? $"{path}\\{filenameStr}" : $"{Path.GetDirectoryName(path)}\\{filenameStr}";

                        RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
                        if (File.Exists(newPath))
                            ConfirmModal("File exists", "Override existing file?",
                                () => Crypto.EncryptFileRSA(path, newPath, rsa.ExportParameters(false)));
                        else
                            Crypto.EncryptFileRSA(path, newPath, rsa.ExportParameters(false));
                    });
                    break;
                }
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }

        public string Name
        {
            get => _name;
            set => Change(ref _name, value);
        }

        public string Value
        {
            get => _value;
            set => Change(ref _value, value);
        }

        public string Description
        {
            get => _description;
            set => Change(ref _description, value);
        }

        public ICommand OnOk
        {
            get => _onOk;
            set => Change(ref _onOk, value);
        }

        // https://docs.microsoft.com/ru-ru/dotnet/csharp/programming-guide/file-system/how-to-copy-delete-and-move-files-and-folders
        private void CopyDir(string fromPath, string toPath, bool replace = true)
        {
            if (!Directory.Exists(toPath))
                Directory.CreateDirectory(toPath);

            var dir = new DirectoryInfo(fromPath);

            var dirs = dir.GetDirectories();
            var files = dir.GetFiles();

            foreach (var file in files)
            {
                var temppath = Path.Combine(toPath, file.Name);
                file.CopyTo(temppath, replace);
            }
            foreach (var subdir in dirs)
            {
                var temppath = Path.Combine(toPath, subdir.Name);
                CopyDir(subdir.FullName, temppath, replace);
            }
        }

        private void MoveFile(bool targetType, string fromPath, string toPath)
        {
            if (targetType)
            {
                if (Directory.Exists(toPath))
                    ConfirmModal("Copy folder", "Replace existing files?",
                        () => CopyDir(fromPath, toPath)
                    );
                else
                    CopyDir(fromPath, toPath, false);
                Directory.Delete(fromPath, true);
            }
            else
            {
                if (File.Exists(toPath))
                    ConfirmModal("Copy file", "Replace existing file?",
                        () => File.Copy(fromPath, toPath, true)
                    );
                else
                    File.Copy(fromPath, toPath, false);
                File.Delete(fromPath);
            }
        }

        private void ConfirmModal(string name, string description, Action onYes, Action onNo = null)
        {
            var result = MessageBox.Show(description, name,
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
                onYes?.Invoke();
            else if (result == DialogResult.No)
                onNo?.Invoke();
        }
    }
}