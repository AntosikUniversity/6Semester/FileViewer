﻿using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using System.Windows.Input;
using FileViewer.Helpers;
using FileViewer.Windows;

namespace FileViewer.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        private readonly CommandFactory _commands;
        private readonly ModalFactory _modalFactory;
        private DirectoryViewModel _openedDirectory;

        public MainViewModel()
        {
            OpenedDirectory = null;

            _commands = new CommandFactory();
            _modalFactory = new ModalFactory();

            InitCommands();
        }

        public Dictionary<string, ICommand> Commands => _commands.Commands;

        public DirectoryViewModel OpenedDirectory
        {
            get => _openedDirectory;
            set => Change(ref _openedDirectory, value);
        }

        private void OpenSpecifiedFile(object obj)
        {
            var filepath = obj as string;
            if (filepath == null || !File.Exists(filepath)) return;

            var openedFile = new FileViewModel(filepath);
            var startedProcess = Process.Start(filepath);
            if (startedProcess != null) return;

            var fileWindow = new FileWindow {DataContext = openedFile};
            openedFile.OpenFile();
            fileWindow.Show();
        }

        private void OpenSpecifiedFolder()
        {
            using (var dialog = new FolderBrowserDialog())
            {
                var result = dialog.ShowDialog();
                if (result == DialogResult.OK)
                    OpenedDirectory = new DirectoryViewModel(dialog.SelectedPath);
            }
        }

        private void InitCommands()
        {
            _commands
                .AddCommand("OpenFolder", OpenSpecifiedFolder)
                .AddCommand("OpenFile", OpenSpecifiedFile)
                .AddCommand("CreateFile", t => _modalFactory.Create(ModalTypesEnum.CreateFile, t))
                .AddCommand("CreateDir", t => _modalFactory.Create(ModalTypesEnum.CreateDirectory, t))
                .AddCommand("Rename", t => _modalFactory.Create(ModalTypesEnum.Rename, t))
                .AddCommand("Copy", t => _modalFactory.Create(ModalTypesEnum.Copy, t))
                .AddCommand("Move", t => _modalFactory.Create(ModalTypesEnum.Move, t))
                .AddCommand("Attributes", t => _modalFactory.Create(ModalTypesEnum.ShowAttributes, t))
                .AddCommand("Observe", t => _modalFactory.Create(ModalTypesEnum.Observe, t))
                .AddCommand("Delete", t => _modalFactory.Create(ModalTypesEnum.Delete, t))
                .AddCommand("TripleDES", t => _modalFactory.Create(ModalTypesEnum.TripleDES, t))
                .AddCommand("Rijndael", t => _modalFactory.Create(ModalTypesEnum.Rijndael, t))
                .AddCommand("RC2", t => _modalFactory.Create(ModalTypesEnum.RC2, t))
                .AddCommand("RSA", t => _modalFactory.Create(ModalTypesEnum.RSA, t))
                .AddCommand("Sign", t => _modalFactory.Create(ModalTypesEnum.Sign, t));
        }
    }
}